//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000010/constant"
	"git.forms.io/isaving/sv/ssv1000010/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000010Controller struct {
    controllers.CommTCCController
}

func (*Ssv1000010Controller) ControllerName() string {
	return "Ssv1000010Controller"
}

// @Desc Ssv1000010Controller Controller
// @Description Entry
// @Param Ssv1000010 body models.SSV1000010I true "body for model content"
// @Success 200 {object} models.SSV1000010O
// @router /SV100010 [post]
// @Date 2020-12-10
func (c *Ssv1000010Controller) Ssv1000010() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000010Controller.Ssv1000010 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000010I := &models.SSV1000010I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000010I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  if err := ssv1000010I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000010 := &services.Ssv1000010Impl{} 
   	ssv1000010.New(c.CommTCCController)
  	ssv1000010.Sv100010I = ssv1000010I
	//ssv1000010.DlsInterface = &commclient.DlsOperate{}
	ssv1000010Compensable := services.Ssv1000010Compensable

	proxy, err := aspect.NewDTSProxy(ssv1000010, ssv1000010Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000010I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD,"DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv1000010O, ok := rsp.(*models.SSV1000010O); ok {
		if responseBody, err := models.PackResponse(ssv1000010O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
} 
// @Title Ssv1000010 Controller
// @Description ssv1000010 controller
// @Param Ssv1000010 body models.SSV1000010I true body for SSV1000010 content
// @Success 200 {object} models.SSV1000010O
// @router /create [post]
func (c *Ssv1000010Controller) SWSsv1000010() {
	//Here is to generate API documentation, no need to implement methods
}
