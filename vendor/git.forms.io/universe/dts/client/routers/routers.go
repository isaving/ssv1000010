//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package routers

import (
	"git.forms.io/universe/dts/client/controllers"
	constant "git.forms.io/universe/solapp-sdk/const"
	"github.com/astaxie/beego"
)

// @Desc initialize call-back routers
func InitDTSRouters() error {
	// atomic transaction confirm/cancel callback
	beego.Router(constant.TXN_CALLBACK_CONFIRM, &controllers.DTSCallbackController{}, "post:AtomicTransactionConfirm")
	beego.Router(constant.TXN_CALLBACK_CANCEL, &controllers.DTSCallbackController{}, "post:AtomicTransactionCancel")

	return nil
}
