package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUSGMI struct {
	//输入是个map
}

type DAACUSGMO struct {

}

type DAACUSGMIDataForm struct {
	FormHead CommonFormHead
	FormData DAACUSGMI
}

type DAACUSGMODataForm struct {
	FormHead CommonFormHead
	FormData DAACUSGMO
}

type DAACUSGMRequestForm struct {
	Form []DAACUSGMIDataForm
}

type DAACUSGMResponseForm struct {
	Form []DAACUSGMODataForm
}

// @Desc Build request message
func (o *DAACUSGMRequestForm) PackRequest(DAACUSGMI DAACUSGMI) (responseBody []byte, err error) {

	requestForm := DAACUSGMRequestForm{
		Form: []DAACUSGMIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUSGMI",
				},
				FormData: DAACUSGMI,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUSGMRequestForm) UnPackRequest(request []byte) (DAACUSGMI, error) {
	DAACUSGMI := DAACUSGMI{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUSGMI, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUSGMI, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUSGMResponseForm) PackResponse(DAACUSGMO DAACUSGMO) (responseBody []byte, err error) {
	responseForm := DAACUSGMResponseForm{
		Form: []DAACUSGMODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUSGMO",
				},
				FormData: DAACUSGMO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUSGMResponseForm) UnPackResponse(request []byte) (DAACUSGMO, error) {

	DAACUSGMO := DAACUSGMO{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUSGMO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUSGMO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUSGMI) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
