package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020006I struct {
	GlobalBizSeqNo	  string
	SrcBizSeqNo		  string
	LoanAcctiAcctNo   string  `valid:"Required;MaxSize(20)"`
	TradeDate         string  `valid:"Required;MaxSize(20)"` //交易日期
	TxnAmt            float64 `valid:"Required"`             //交易金额
	CurCd             string  `valid:"Required;MaxSize(3)"`
	RcrdacctAcctnDt   string  `valid:"MaxSize(10)"`
	RcrdacctAcctnTm   string  `valid:"MaxSize(20)"`
	LiqdDt            string  `valid:"MaxSize(8)"`
	LiqdTm            string  `valid:"MaxSize(8)"`
	TxOrgNo           string  `valid:"MaxSize(4)"`
	AgenOrgNo         string  `valid:"MaxSize(4)"`
	TxTelrNo          string  `valid:"MaxSize(6)"`
	AuthTelrNo        string  `valid:"MaxSize(6)"`
	RchkTelrNo        string  `valid:"MaxSize(6)"`
	LunchChnlTypCd    string  `valid:"MaxSize(4)"`
	AccessChnlTypCd   string  `valid:"MaxSize(4)"`
	TerminalNo        string  `valid:"MaxSize(20)"`
	BizSysNo          string  `valid:"MaxSize(3)"`
	TranCd            string  `valid:"MaxSize(10)"`
	LiqdBizTypCd      string  `valid:"MaxSize(2)"`
	BizKindCd         string  `valid:"MaxSize(3)"`
	BizClsfCd         string  `valid:"MaxSize(20)"`
	DebitCrdtFlg      string  `valid:"MaxSize(1)"`
	MbankFlg          string  `valid:"MaxSize(1)"`
	StateAndRgnCd     string  `valid:"MaxSize(4)"`
	OthbnkBnkNo       string  `valid:"MaxSize(14)"`
	OpenAcctOrgNo     string  `valid:"MaxSize(4)"`
	MbankAcctFlg      string  `valid:"MaxSize(1)"`
	TxDtlTypCd        string  `valid:"MaxSize(1)"`
	CustNo            string  `valid:"MaxSize(14)"`
	MerchtNo          string  `valid:"MaxSize(32)"`
	SvngAcctNo        string  `valid:"MaxSize(32)"`
	CardNoOrAcctNo    string  `valid:"MaxSize(32)"`
	FinTxAmtTypCd     string  `valid:"MaxSize(1)"`
	CashrmtFlgCd      string  `valid:"MaxSize(1)"`
	AcctBal           float64
	CashTxRcptpymtCd  string `valid:"MaxSize(1)"`
	FrnexcgStlManrCd  string `valid:"MaxSize(8)"`
	PostvReblnTxFlgCd string `valid:"MaxSize(1)"`
	RvrsTxFlgCd       string `valid:"MaxSize(1)"`
	OrginlTxAcctnDt   string `valid:"MaxSize(8)"`
	OrginlHostTxSn    string `valid:"MaxSize(10)"`
	OrginlTxTelrNo    string `valid:"MaxSize(6)"`
	CustMgrTelrNo     string `valid:"MaxSize(6)"`
	LoanUsageCd       string `valid:"MaxSize(4)"`
	AcctBookNo        string `valid:"MaxSize(15)"`
	PmitRvrsFlg       string `valid:"MaxSize(1)"`
	ChnlRvrsCtrlFlgCd string `valid:"MaxSize(4)"`
	GvayLiqdFlg       string `valid:"MaxSize(1)"`
	MndArapFlg        string `valid:"MaxSize(1)"`
	ArrRecCnt         int
	Records           []interface{} `valid:"Required"`
	ValueDate			string
	MessageCode			string
	AbstractCode		string
	Abstract			string
}

type AC020006O struct {
	AcctgAcctNo		string
	Currency		string
	Status			string
	Balance			float64
	ArrRecCnt		int
	Records			[]interface{}
}

type AC020006IDataForm struct {
	FormHead CommonFormHead
	FormData AC020006I
}

type AC020006ODataForm struct {
	FormHead CommonFormHead
	FormData AC020006O
}

type AC020006RequestForm struct {
	Form []AC020006IDataForm
}

type AC020006ResponseForm struct {
	Form []AC020006ODataForm
}

// @Desc Build request message
func (o *AC020006RequestForm) PackRequest(AC020006I AC020006I) (responseBody []byte, err error) {

	requestForm := AC020006RequestForm{
		Form: []AC020006IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020006I",
				},
				FormData: AC020006I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC020006RequestForm) UnPackRequest(request []byte) (AC020006I, error) {
	AC020006I := AC020006I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC020006I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020006I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC020006ResponseForm) PackResponse(AC020006O AC020006O) (responseBody []byte, err error) {
	responseForm := AC020006ResponseForm{
		Form: []AC020006ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020006O",
				},
				FormData: AC020006O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC020006ResponseForm) UnPackResponse(request []byte) (AC020006O, error) {

	AC020006O := AC020006O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC020006O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC020006O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC020006I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
