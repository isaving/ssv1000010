package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUBC0I struct {
	//输入是个map
}

type DAACUBC0O struct {

}

type DAACUBC0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUBC0I
}

type DAACUBC0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUBC0O
}

type DAACUBC0RequestForm struct {
	Form []DAACUBC0IDataForm
}

type DAACUBC0ResponseForm struct {
	Form []DAACUBC0ODataForm
}

// @Desc Build request message
func (o *DAACUBC0RequestForm) PackRequest(DAACUBC0I DAACUBC0I) (responseBody []byte, err error) {

	requestForm := DAACUBC0RequestForm{
		Form: []DAACUBC0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUBC0I",
				},
				FormData: DAACUBC0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUBC0RequestForm) UnPackRequest(request []byte) (DAACUBC0I, error) {
	DAACUBC0I := DAACUBC0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUBC0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUBC0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUBC0ResponseForm) PackResponse(DAACUBC0O DAACUBC0O) (responseBody []byte, err error) {
	responseForm := DAACUBC0ResponseForm{
		Form: []DAACUBC0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUBC0O",
				},
				FormData: DAACUBC0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUBC0ResponseForm) UnPackResponse(request []byte) (DAACUBC0O, error) {

	DAACUBC0O := DAACUBC0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUBC0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUBC0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUBC0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
