package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACBAN0I struct {
	TransactionCode string `validate:"required,max=10"`
	Describe        string
	AcctType        string
	AcctIntl        string
	IsRealTime      string
}

type DAACBAN0O struct {
	TransactionCode string
	Describe        string
	AcctType        string
	AcctIntl        string
	IsRealTime      string
}

type DAACBAN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACBAN0I
}

type DAACBAN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACBAN0O
}

type DAACBAN0RequestForm struct {
	Form []DAACBAN0IDataForm
}

type DAACBAN0ResponseForm struct {
	Form []DAACBAN0ODataForm
}

// @Desc Build request message
func (o *DAACBAN0RequestForm) PackRequest(DAACBAN0I DAACBAN0I) (responseBody []byte, err error) {

	requestForm := DAACBAN0RequestForm{
		Form: []DAACBAN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACBAN0I",
				},
				FormData: DAACBAN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACBAN0RequestForm) UnPackRequest(request []byte) (DAACBAN0I, error) {
	DAACBAN0I := DAACBAN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACBAN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACBAN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACBAN0ResponseForm) PackResponse(DAACBAN0O DAACBAN0O) (responseBody []byte, err error) {
	responseForm := DAACBAN0ResponseForm{
		Form: []DAACBAN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACBAN0O",
				},
				FormData: DAACBAN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACBAN0ResponseForm) UnPackResponse(request []byte) (DAACBAN0O, error) {

	DAACBAN0O := DAACBAN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACBAN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACBAN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACBAN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
