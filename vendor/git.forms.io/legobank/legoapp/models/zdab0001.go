package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type ZDAB0001I struct {
	Total			int		//笔数
	Number			int		//协程的个数
	LoanProdtNo		string	//产品ID
	ProdName		string	// 产品名称
	CustNo			string	//客户号
	LoanAmt     	float64 // 贷款金额
	PeriodNum   	int		// 分期期数
	CustName		string	// 客户姓名

	GuarMethod 		string	// 担保方式1-No Guarantee 2-Guarantor 如果选择1，则担保人姓名，证件类型，证件号码，护照国家都无需输入  如果选择2，则全部都要输入
	LoanPur			string	//贷款目的
	IntRate			float64	// 利率
	CreditLimit		float64	// 授信总额度
	CreditUsage		float64	// 总额度
	AppDt           string	// 申请日期
	DrawDt          string	// 放款日期
	MaturDt         string	// 到期日期
	RepayDay        string	// 还款日
	DisburStatus    string	// 放款状态
	TransferChannel string	// 放款渠道
	RepayFreq       string	// 还款周期
	AuotoRepyFlag   string	// 自动还款标识
	AppFee         float64	// 申请费用
	ProcFee        float64	// 处理费
	OverFee        float64	// 逾期费用
	AnticipatedFee float64	// 提前还款费用
	ExtensionFee   float64	// 展期费用
	RepayPlanFee   float64	// 还款计划调整费用
	Tax            float64	// 税
	Maker          string 	// 申请人员工号
	MakerComment   string 	// 放款说明
	MakelnAplySn   string 	// 放款申请流水号

}

type ZDAB0001O struct {
	MakelnAplySn    string //放款申请流水号
	LoanDubilNo		string //贷款借据号
	CustNo          string //客户编号
	LoanStatus      string //贷款状态
	ApproveUserId   string //审批人员工号
	ApproveView     string //审批意见
	ApproveComment  string //审批意见说明
}

type ZDAB0001IDataForm struct {
	FormHead CommonFormHead
	FormData ZDAB0001I
}

type ZDAB0001ODataForm struct {
	FormHead CommonFormHead
	FormData ZDAB0001O
}

type ZDAB0001RequestForm struct {
	Form []ZDAB0001IDataForm
}

type ZDAB0001ResponseForm struct {
	Form []ZDAB0001ODataForm
}

// @Desc Build request message
func (o *ZDAB0001RequestForm) PackRequest(ZDAB0001I ZDAB0001I) (responseBody []byte, err error) {

	requestForm := ZDAB0001RequestForm{
		Form: []ZDAB0001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "ZDAB0001I",
				},
				FormData: ZDAB0001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *ZDAB0001RequestForm) UnPackRequest(request []byte) (ZDAB0001I, error) {
	ZDAB0001I := ZDAB0001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return ZDAB0001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return ZDAB0001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *ZDAB0001ResponseForm) PackResponse(ZDAB0001O ZDAB0001O) (responseBody []byte, err error) {
	responseForm := ZDAB0001ResponseForm{
		Form: []ZDAB0001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "ZDAB0001O",
				},
				FormData: ZDAB0001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *ZDAB0001ResponseForm) UnPackResponse(request []byte) (ZDAB0001O, error) {

	ZDAB0001O := ZDAB0001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return ZDAB0001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return ZDAB0001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

