//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000010/constant"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
    dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv1000010Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000010",
	ConfirmMethod: "ConfirmSsv1000010",
	CancelMethod:  "CancelSsv1000010",
}

type Ssv1000010 interface {
	TrySsv1000010(*models.SSV1000010I) (*models.SSV1000010O, error)
	ConfirmSsv1000010(*models.SSV1000010I) (*models.SSV1000010O, error)
	CancelSsv1000010(*models.SSV1000010I) (*models.SSV1000010O, error)
}

type Ssv1000010Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
    Sv100010O         *models.SSV1000010O
    Sv100010I         *models.SSV1000010I
}
// @Desc Ssv1000010 process
// @Author
// @Date 2020-12-04
func (impl *Ssv1000010Impl) TrySsv1000010(ssv1000010I *models.SSV1000010I) (ssv1000010O *models.SSV1000010O, err error) {

	impl.Sv100010I = ssv1000010I

	//新增客户合约关系
	if err :=impl.CU000005(); nil != err {
		return nil, err
	}

	ssv1000010O = &models.SSV1000010O{
	}

	return ssv1000010O, nil
}

func (impl *Ssv1000010Impl) ConfirmSsv1000010(ssv1000010I *models.SSV1000010I) (ssv1000010O *models.SSV1000010O, err error) {
	log.Debug("Start confirm ssv1000010")
	return nil, nil
}

func (impl *Ssv1000010Impl) CancelSsv1000010(ssv1000010I *models.SSV1000010I) (ssv1000010O *models.SSV1000010O, err error) {
	log.Debug("Start cancel ssv1000010")
	return nil, nil
}

func (impl *Ssv1000010Impl) CU000005() error {
	rspBody, err :=impl.Sv100010I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	//CU000005 --新增客户合约关系
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.CU000005, rspBody);
	if err != nil{
		log.Errorf("CU000005, err:%v",err)
		return err
	}
	if err := impl.Sv100010O.UnPackResponse(resBody); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	return nil
}