package services

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/models"
	jsoniter "github.com/json-iterator/go"
	. "reflect"
	"testing"
)

func (this *Ssv1000010Impl) RequestSyncServiceWithDCN(dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

func (this *Ssv1000010Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

func (this *Ssv1000010Impl) RequestServiceWithDCN(dstDcn, serviceKey string, requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	responseData = []byte(`{"Code":0,"Data":{"BS-LEGO":["C03101"]},"Message":"","errorCode":0,"errorMsg":"","response":{"BS-LEGO":["C03101"]}}`)
	return
}

func TestService(t *testing.T) {
	runTest(Ssv1000010Impl{}, "TrySsv1000010") //这两个都要改哦
	runTest(Ssv1000010Impl{}, "CancelSsv1000010")
	runTest(Ssv1000010Impl{}, "ConfirmSsv1000010")
}

//报文头
var SrcAppProps = map[string]string{
	"TxUserId":   "1001",
	"TxDeptCode": "1001",
}

//输入请求
var request = []models.SSV1000010I{
	{
		CustId:   "1001",
		Account: "1001",
		ContactType: "1001",
		AListContact: []models.SSV1000010IAListContact{
			{
				ContactType:"1",
				Contact:"1",
				Name:"1",
			},
		},
		AListAddr : []models.SSV1000010IAListAddr{
					{
						AddrType:"2",
					},
		},
		ObjPublic: models.SSV1000010IObjPublic{
			BranchNo:"22",
			TellerId:"2",
		},

	},
	{
		CustId:   "100133",
		Account: "1001",
		AListContact: []models.SSV1000010IAListContact{
			{
				ContactType:"1",
				Contact:"1",
				Name:"1",
			},
		},
		AListAddr : []models.SSV1000010IAListAddr{
			{
				AddrType:"2",
			},
		},
		ObjPublic: models.SSV1000010IObjPublic{
			BranchNo:"22",
			TellerId:"2",
		},

	},
}

/* []models.IL120010I{
	{
		LoanDubilNo: "123312313123",
		NewMatrDt:   "20200820",
	},
	{
		LoanDubilNo: "123123123123",
		NewMatrDt:   "20200820",
	},
}*/

//使用反引号可以直接换行 还不会被带双引号影响
var response = map[string]interface{}{
	//这个key 是你调用RequestSyncServiceElementKey()这个函数传的serviceKey的字符串 不一定是topic 好好看看自己的代码传的字符串是啥
	//可以直接粘贴报文 也可以写一个返回结构体
	"CU000005": `{"errorCode":"0","errorMsg":"success","response":{"AccountingId":"1"}}`,

	/*"DAACRTC3": models.DAACRTC3O{
		LogTotCount: "1",
		Records: []models.DAACRTC3ORecords{{
			AccmCmpdAmt:    0,
			AccmIntSetlAmt: 0,
			AccmWdAmt:      0,
			AcctgAcctNo:    "",
			AcruUnstlIntr:  0,
			RecordNo:       0,
			RepaidInt:      0,
			RepaidIntAmt:   0,
			Status:         "",
			UnpaidInt:      0,
			UnpaidIntAmt:   0,
		}},
	},
	"DAILUDK1": `{"Form":[{"FormData":{"State":"ok"}}]}`,
	"DAILRDK3": models.DAILRDK3O{
		Records: []models.DAILRDK3ORecords{{
			LoanDubilNo:           "adfasdfasdf",
			RecalDt:               "",
			SeqNo:                 0,
			CustNo:                "",
			RemainPrin:            0,
			ActlstPrin:            0,
			ActlstIntr:            0,
			Pridnum:               0,
			KeprcdStusCd:          "",
			CurrPeriodRecalFlg:    "",
			CurrPeriodIntacrBgnDt: "",
			CurrPeriodIntacrEndDt: "",
			RepayDt:               "",
			PlanRepayTotlAmt:      0,
			PlanRepayPrin:         0,
			PlanRepayIntr:         0,
		}},
		PageNo:       1,
		PageRecCount: 10,
	},
	"DAACRAN1": models.DAACRAN1O{
		Balance: 0,
	},

	"IL1QS088": models.IL1QS088O{
		Records: []models.IL1QS088ORecords{{
			Pridnum: 0,
		}},
		TotCount: 1,
	},

	"DAILUDK3": `{"Form":[{"FormData":{"State":"ok"}}]}`,
	"DAILCDK3": `{"Form":[{"FormData":{"State":"ok"}}]}`,

	"DAILRFK1": models.DAILRFK1O{Records: []models.DAILRFK1ORecords{{
		MakelnAplySn: "",
	}}},

	"DAILUFK1": `{"Form":[{"FormData":{"State":"ok"}}]}`,*/
}

//func (this *Ssv1000010Impl)RequestServiceWithDCN(dstDcn, serviceKey string,requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
//	if serviceKey == "DlsuDcnLists" {
//
//	}
//}

var errServiceKey []string

//初始化
func init() {
	for serviceKey := range response {
		errServiceKey = append(errServiceKey, serviceKey)
	}
}

var currentIndex = 0
var passed bool

//排列组合返回报文
func RequestService(serviceKey string, reqBytes []byte) (responseData []byte, err error) {
	if serviceKey == errServiceKey[currentIndex] && passed {
		currentIndex++
		if currentIndex >= len(errServiceKey) {
			currentIndex = 0
		}
		return nil, errors.New("error")
	}
	switch response[serviceKey].(type) {
	case string:
		responseData = []byte(response[serviceKey].(string))
	default:
		responseData = getByte(response[serviceKey])
	}
	return
}

func getByte(v interface{}) (bt []byte) {
	bt, _ = jsoniter.Marshal(struct {
		Form [1]struct{ FormData interface{} }
	}{Form: [1]struct{ FormData interface{} }{{v}}})
	return
}

func runTest(serviceStruct interface{}, method string) {
	param := make([]Value, 1)
	for _, req := range request {
		param[0] = ValueOf(&req)
		passed = false
		for i := 0; i < len(response); i++ {
			callMethod(serviceStruct, method, param)
			passed = true
		}
	}

}

func callMethod(serviceStruct interface{}, method string, params []Value) {
	service := New(TypeOf(serviceStruct))
	service.Elem().FieldByName("SrcAppProps").Set(ValueOf(SrcAppProps))
	ret := service.MethodByName(method).Call(params)
	fmt.Printf(" 返回 [ %v ] \n 错误 [ %v ]\n\n", ret[0], ret[1])
}

